const mensClothingCategory = document.querySelector('.mens-cloth-container');
const jeweleryCategory = document.querySelector('.jewelery-container');
const electronicsCategory = document.querySelector('.electronics-container');
const signupButton = document.getElementById('signup');
const womensClothingCategory = document.querySelector('.womens-cloth-container');
const loader = document.querySelector('.loader-container')

loader.style.display='block'

function createProductElement(id, title, price, description, image, rating) {
    const productDiv = document.createElement('div');
    productDiv.className = 'product';
    productDiv.id = id;

    const imageDiv = document.createElement('div');
    imageDiv.className = 'image-div';

    const imageElement = document.createElement('img');
    imageElement.src = image;
    imageElement.alt = '';

    imageDiv.appendChild(imageElement);
    productDiv.appendChild(imageDiv);

    const productDetailsDiv = document.createElement('div');
    productDetailsDiv.className = 'product-details';

    const titleDiv = document.createElement('div');
    titleDiv.className = 'title';

    const titleHeading = document.createElement('h3');
    titleHeading.textContent = title;

    titleDiv.appendChild(titleHeading);
    productDetailsDiv.appendChild(titleDiv);

    const priceRatingDiv = document.createElement('div');
    priceRatingDiv.className = 'price-rating';

    const priceSpan = document.createElement('span');
    priceSpan.className = 'price';
    priceSpan.textContent = `$${price}`;

    priceRatingDiv.appendChild(priceSpan);

    const ratingDiv = document.createElement('div');
    ratingDiv.className = 'rating';

    const rateDiv = document.createElement('div');
    rateDiv.className = 'rate';

    const rateSpan = document.createElement('span');
    rateSpan.textContent = rating.rate;

    const starIcon = document.createElement('i');
    starIcon.className = 'fa-solid fa-star fa-lg fa-small';
    starIcon.style.color = '#1962f5';

    rateDiv.appendChild(rateSpan);
    rateDiv.appendChild(starIcon);
    ratingDiv.appendChild(rateDiv);

    const ratedUsersDiv = document.createElement('div');
    ratedUsersDiv.className = 'rated-users';

    const ratedUsersSpan = document.createElement('span');
    ratedUsersSpan.textContent = rating.count;

    ratedUsersDiv.appendChild(ratedUsersSpan);
    ratingDiv.appendChild(ratedUsersDiv);

    priceRatingDiv.appendChild(ratingDiv);
    productDetailsDiv.appendChild(priceRatingDiv);

    productDiv.appendChild(productDetailsDiv);

    return productDiv;
}

fetch('https://fakestoreapi.com/products')
    .then(res => res.json())
    .then(json => {
        loader.style.display = 'none'
        const allCategories = json.reduce((output, product) => {
            if (!output.includes(product.category)) {
                output.push(product.category);
            }
            return output;
        }, []);

        for (let product of json) {
            const { id, title, price, description, image, rating } = product;
            let categoryContainer;

            if (product.category === "men's clothing") {
                categoryContainer = mensClothingCategory;
            } else if (product.category === "women's clothing") {
                categoryContainer = womensClothingCategory;
            } else if (product.category === "electronics") {
                categoryContainer = electronicsCategory;
            } else if (product.category === "jewelery") {
                categoryContainer = jeweleryCategory;
            }

            const productElement = createProductElement(id, title, price, description, image, rating);
        

            categoryContainer.appendChild(productElement);
        }
       
    }).catch(error=>{
       window.location.href = './error-page.html'
    })

signupButton.addEventListener('click', () => {
    window.location.href = "./signup/signup.html";
});
