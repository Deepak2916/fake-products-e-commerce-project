
const form = document.querySelector('form');
const firstNameInput = document.getElementById('first-name');
const lastNameInput = document.getElementById('last-name');
const emailInput = document.getElementById('email');
const passwordInput = document.getElementById('password');
const confirmPasswordInput = document.getElementById('confirm-password');
const homeButton = document.getElementById('home')


homeButton.addEventListener('click',()=>{
  console.log('dsf');
  window.location.href = '../index.html'
})

form.addEventListener('submit', function (event) {
  event.preventDefault();

  validateFirstName();
  validateLastName();
  validateEmail();
  validatePassword();
  validateConfirmPassword();

 
  if (isFormValid()) {
    console.log('Form submitted successfully!')
    console.log(document.getElementById('success-message').style);

  }
});



function validateFirstName() {
  const firstNameValue = firstNameInput.value.trim();
  const hasNumbersAndSpecialChars = /[0-9!@#$%^&*(),.?":{}|<>]/.test(firstNameValue)

  if (hasNumbersAndSpecialChars || firstNameValue === "") {
    displayError(firstNameInput, 'enter valid first name');

  } else {
    removeError(firstNameInput);
  }
}


function validateLastName() {
  const lastNameValue = lastNameInput.value.trim();
  const hasNumbersAndSpecialChars = /[0-9!@#$%^&*(),.?":{}|<>]/.test(lastNameValue)


  if (hasNumbersAndSpecialChars || lastNameValue === '') {
    displayError(lastNameInput, 'enter valid last name');
  } else {
    removeError(lastNameInput);
  }
}

function validateEmail() {
  const emailValue = emailInput.value.trim();
 
  if (emailValue === '' || emailValue.includes('@')===false) {
    displayError(emailInput, 'enter valid email');
  } else {
    removeError(emailInput);
  }
}


function validatePassword() {
  const passwordValue = passwordInput.value.trim();

  const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

  if (passwordValue === '') {
    displayError(passwordInput, 'enter valid password');
  } else if (!passwordPattern.test(passwordValue)) {
    displayError(passwordInput, 'password should conatain minimum 8 charecters and include upper case, lower case, numbers, and special characters');
  } else {
    removeError(passwordInput);
  }
}


function validateConfirmPassword() {
  const passwordValue = passwordInput.value.trim();
  const confirmPasswordValue = confirmPasswordInput.value.trim();

  if (confirmPasswordValue === '') {
    displayError(confirmPasswordInput, 'password not matched');
  } else if (passwordValue !== confirmPasswordValue) {
    displayError(confirmPasswordInput, 'password not matched');
  } else {
    removeError(confirmPasswordInput);
  }
}





function displayError(inputElement, errorMessage) {
  const errorElement = inputElement.nextElementSibling;
  errorElement.textContent = errorMessage;
  errorElement.style.display = 'block';
}


function removeError(inputElement) {
  const errorElement = inputElement.nextElementSibling;
  errorElement.textContent = '';

  errorElement.style.display = 'none';
}

function isFormValid() {
  const errorMessages = form.querySelectorAll('p');
  for (let i = 0; i < errorMessages.length; i++) {
    if (errorMessages[i].textContent !== '') {
      return false;
    }
  }
  return true;
}
